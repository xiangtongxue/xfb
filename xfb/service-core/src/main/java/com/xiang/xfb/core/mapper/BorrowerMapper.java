package com.xiang.xfb.core.mapper;

import com.xiang.xfb.core.pojo.entity.Borrower;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 借款人 Mapper 接口
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
public interface BorrowerMapper extends BaseMapper<Borrower> {

}
