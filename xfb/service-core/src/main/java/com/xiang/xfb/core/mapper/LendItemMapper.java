package com.xiang.xfb.core.mapper;

import com.xiang.xfb.core.pojo.entity.LendItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 标的出借记录表 Mapper 接口
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
public interface LendItemMapper extends BaseMapper<LendItem> {

}
