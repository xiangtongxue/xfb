package com.xiang.xfb.core.service.impl;

import com.xiang.xfb.core.pojo.entity.UserAccount;
import com.xiang.xfb.core.mapper.UserAccountMapper;
import com.xiang.xfb.core.service.UserAccountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户账户 服务实现类
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
@Service
public class UserAccountServiceImpl extends ServiceImpl<UserAccountMapper, UserAccount> implements UserAccountService {

}
