package com.xiang.xfb.core.mapper;

import com.xiang.xfb.core.pojo.entity.UserAccount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户账户 Mapper 接口
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
public interface UserAccountMapper extends BaseMapper<UserAccount> {

}
