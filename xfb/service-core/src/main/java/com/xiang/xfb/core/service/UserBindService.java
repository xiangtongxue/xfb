package com.xiang.xfb.core.service;

import com.xiang.xfb.core.pojo.entity.UserBind;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiang.xfb.core.pojo.vo.UserBindVo;

import java.util.Map;

/**
 * <p>
 * 用户绑定表 服务类
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
public interface UserBindService extends IService<UserBind> {

    String commitBindUser(UserBindVo userBindVO, Long userId);

    void notify(Map<String, Object> paramMap);
}
