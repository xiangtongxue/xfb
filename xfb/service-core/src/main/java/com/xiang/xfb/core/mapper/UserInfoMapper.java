package com.xiang.xfb.core.mapper;

import com.xiang.xfb.core.pojo.entity.UserInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户基本信息 Mapper 接口
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
public interface UserInfoMapper extends BaseMapper<UserInfo> {

}
