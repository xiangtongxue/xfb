package com.xiang.xfb.core.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.xiang.xfb.core.mapper.DictMapper;
import com.xiang.xfb.core.pojo.dto.ExcelDictDTO;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@NoArgsConstructor
public class ExcelDictDTOListener extends AnalysisEventListener<ExcelDictDTO> {

    private DictMapper dictMapper;

    List<ExcelDictDTO>  list =  new ArrayList<>();

    // 5条记录存储一次
    private static final int BATCH_COUNT = 5;

    public ExcelDictDTOListener(DictMapper dictMapper){
        this.dictMapper = dictMapper;
    }

    /**
     * 这个每一条数据解析都会来调用
     *
     * @param excelDictDTO    one row value. Is is same as {@link AnalysisContext#readRowHolder()}
     * @param analysisContext
     */

    @Override
    public void invoke(ExcelDictDTO excelDictDTO, AnalysisContext analysisContext) {
        log.info("解析到一条数据",excelDictDTO);
        //将数据存入到数据列表
        list.add(excelDictDTO);
        if (list.size() >= BATCH_COUNT){
            saveData();
            list.clear();
        }
        //mapper层save
    }

    /**
     * 所有数据解析完成了 都会来调用
     *
     * @param analysisContext
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        // 最后剩余的数据记录不足BATCH_COUNT，最终一次性存储剩余数据
        saveData();
        log.info("exce所有数据解析完成");
    }

    public void saveData(){
        log.info("存储数据条数：{}",list.size());
        dictMapper.insertBatch(list);
    }
}
