package com.xiang.xfb.core.service;

import com.xiang.xfb.core.pojo.entity.IntegralGrade;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 积分等级表 服务类
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
public interface IntegralGradeService extends IService<IntegralGrade> {

}
