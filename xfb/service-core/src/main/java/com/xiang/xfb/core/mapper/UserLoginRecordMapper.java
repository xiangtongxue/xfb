package com.xiang.xfb.core.mapper;

import com.xiang.xfb.core.pojo.entity.UserLoginRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户登录记录表 Mapper 接口
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
public interface UserLoginRecordMapper extends BaseMapper<UserLoginRecord> {

}
