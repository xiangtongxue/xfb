package com.xiang.xfb.core.service;

import com.xiang.xfb.core.pojo.entity.LendReturn;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 还款记录表 服务类
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
public interface LendReturnService extends IService<LendReturn> {

}
