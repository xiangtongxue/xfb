package com.xiang.xfb.core.controller.admin;


import com.alibaba.excel.EasyExcel;
import com.alibaba.fastjson.JSON;
import com.xiang.common.exception.BusinessException;
import com.xiang.common.result.R;
import com.xiang.common.result.ResponseEnum;
import com.xiang.xfb.core.pojo.dto.ExcelDictDTO;
import com.xiang.xfb.core.pojo.entity.Dict;
import com.xiang.xfb.core.service.DictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import jdk.nashorn.internal.runtime.linker.LinkerCallSite;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 数据字典 前端控制器
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
@Api(tags = "数据字段管理")
@Slf4j
@RestController
@RequestMapping("/admin/core/dict")
public class AdminDictController {

    @Resource
    DictService dictService;

    @ApiOperation("导入excel")
    @PostMapping("/import")
    public R batchImport(@RequestParam("file") MultipartFile file){
        try {
            dictService.importData(file.getInputStream());
            return R.oK().message("导入成功");
        } catch (IOException e) {
            throw new BusinessException(ResponseEnum.UPLOAD_ERROR,e);
        }
    }

    /**
     * 文件下载并且失败的时候返回json（默认失败了会返回一个有部分数据的Excel）
     *
     * @since 2.1.1
     */
    @ApiOperation("excel数据导出")
    @GetMapping("/export")
    public void downloadFailedUsingJson(HttpServletResponse response) throws IOException {
        // 这里注意 有同学反应使用swagger 会导致各种问题，请直接用浏览器或者用postman
        try {
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setCharacterEncoding("utf-8");
            // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
            String fileName = URLEncoder.encode("测试字典下载", "UTF-8").replaceAll("\\+", "%20");
            response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
            // 这里需要设置不关闭流
            EasyExcel.write(response.getOutputStream(), ExcelDictDTO.class).autoCloseStream(Boolean.FALSE).sheet("模板")
                    .doWrite(dictService.listDictData());
        } catch (Exception e) {
            // 重置response
            response.reset();
            response.setContentType("application/json");
            response.setCharacterEncoding("utf-8");
            Map<String, String> map = new HashMap<>();
            map.put("code", "200");
            map.put("message", "下载文件失败" + e.getMessage());
            response.getWriter().println(JSON.toJSONString(map));
        }
    }


    @ApiOperation("根据上级id获取子节点数据列表")
    @GetMapping("/listByParentId/{parentId}")
    public R listByParentId(@ApiParam(value = "上级结点id",required = true) @PathVariable Long parentId){
         List<Dict> dictList = dictService.listByParentId(parentId);
        return R.oK().data("list",dictList);
    }
}

