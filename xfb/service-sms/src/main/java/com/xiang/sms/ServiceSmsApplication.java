package com.xiang.sms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * DataSourceAutoConfiguration会自动加载.
 *
 * 没有配置spring - datasource - url 属性.
 *
 * spring - datasource - url 配置的地址格式有问题.
 *
 * 配置 spring - datasource - url的文件没有加载.
 */

@EnableFeignClients
@SpringBootApplication(exclude= {DataSourceAutoConfiguration.class})
@ComponentScan({"com.xiang","com.xiang.common"})
public class ServiceSmsApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceSmsApplication.class, args);
    }
}
