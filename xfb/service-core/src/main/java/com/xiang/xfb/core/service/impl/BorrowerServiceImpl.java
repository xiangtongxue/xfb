package com.xiang.xfb.core.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiang.xfb.core.enums.BorrowerStatusEnum;
import com.xiang.xfb.core.enums.IntegralEnum;
import com.xiang.xfb.core.mapper.BorrowerAttachMapper;
import com.xiang.xfb.core.mapper.UserInfoMapper;
import com.xiang.xfb.core.mapper.UserIntegralMapper;
import com.xiang.xfb.core.pojo.entity.Borrower;
import com.xiang.xfb.core.mapper.BorrowerMapper;
import com.xiang.xfb.core.pojo.entity.BorrowerAttach;
import com.xiang.xfb.core.pojo.entity.UserInfo;
import com.xiang.xfb.core.pojo.entity.UserIntegral;
import com.xiang.xfb.core.pojo.vo.BorrowerApprovalVo;
import com.xiang.xfb.core.pojo.vo.BorrowerAttachVo;
import com.xiang.xfb.core.pojo.vo.BorrowerDetailVo;
import com.xiang.xfb.core.pojo.vo.BorrowerVo;
import com.xiang.xfb.core.service.BorrowerAttachService;
import com.xiang.xfb.core.service.BorrowerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiang.xfb.core.service.DictService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 借款人 服务实现类
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
@Service
public class BorrowerServiceImpl extends ServiceImpl<BorrowerMapper, Borrower> implements BorrowerService {
    @Resource
    private BorrowerAttachMapper borrowerAttachMapper;

    @Resource
    private UserInfoMapper userInfoMapper;

    @Resource
    private DictService dictService;

    @Resource
    private BorrowerAttachService borrowerAttachService;


    @Resource
    private UserIntegralMapper userIntegralMapper;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveBorrowerVoByUserId(BorrowerVo borrowerVo, Long userId) {

        UserInfo userInfo = userInfoMapper.selectById(userId);

        //保存借款人信息
        Borrower borrower = new Borrower();
        BeanUtils.copyProperties(borrowerVo, borrower);
        borrower.setUserId(userId);
        borrower.setName(userInfo.getName());
        borrower.setIdCard(userInfo.getIdCard());
        borrower.setMobile(userInfo.getMobile());
        borrower.setStatus(BorrowerStatusEnum.AUTH_RUN.getStatus());//认证中
        baseMapper.insert(borrower);

        //保存附件
        List<BorrowerAttach> borrowerAttachList = borrowerVo.getBorrowerAttachList();
        borrowerAttachList.forEach(borrowerAttach -> {
            borrowerAttach.setBorrowerId(borrower.getId());
            borrowerAttachMapper.insert(borrowerAttach);
        });

        //更新会员状态，更新为认证中
        userInfo.setBorrowAuthStatus(BorrowerStatusEnum.AUTH_RUN.getStatus());
        userInfoMapper.updateById(userInfo);
    }

    @Override
    public Integer getStatusByUserId(Long userId) {

        QueryWrapper<Borrower> borrowerQueryWrapper = new QueryWrapper<>();
        borrowerQueryWrapper.select("status").eq("user_id", userId);
        List<Object> objects = baseMapper.selectObjs(borrowerQueryWrapper);

        if(objects.size() == 0){
            //借款人尚未提交信息
            return BorrowerStatusEnum.NO_AUTH.getStatus();
        }
        Integer status = (Integer)objects.get(0);
        return status;
    }

    @Override
    public IPage<Borrower> listPage(Page<Borrower> pageParam, String keyword) {

        QueryWrapper<Borrower> borrowerQueryWrapper = new QueryWrapper<>();

        if(StringUtils.isEmpty(keyword)){
            return baseMapper.selectPage(pageParam, null);
        }

        borrowerQueryWrapper.like("name", keyword)
                .or().like("id_card", keyword)
                .or().like("mobile", keyword)
                .orderByDesc("id");
        return baseMapper.selectPage(pageParam, borrowerQueryWrapper);
    }

    @Override
    public BorrowerDetailVo getBorrowerDetailVOById(Long id) {

        //获取借款人信息
        Borrower borrower = baseMapper.selectById(id);

        //填充基本借款人信息
        BorrowerDetailVo borrowerDetailVo = new BorrowerDetailVo();
        BeanUtils.copyProperties(borrower, borrowerDetailVo);

        //婚否
        borrowerDetailVo.setMarry(borrower.getMarry()?"是":"否");
        //性别
        borrowerDetailVo.setSex(borrower.getSex()==1?"男":"女");

        //查詢下拉列表选中内容
        String education = dictService.getNameByParentDictCodeAndValue("education", borrower.getEducation());
        String industry = dictService.getNameByParentDictCodeAndValue("moneyUse", borrower.getIndustry());
        String income = dictService.getNameByParentDictCodeAndValue("income", borrower.getIncome());
        String returnSource = dictService.getNameByParentDictCodeAndValue("returnSource", borrower.getReturnSource());
        String contactsRelation = dictService.getNameByParentDictCodeAndValue("relation", borrower.getContactsRelation());

        //设置下拉列表选中内容
        borrowerDetailVo.setEducation(education);
        borrowerDetailVo.setIndustry(industry);
        borrowerDetailVo.setIncome(income);
        borrowerDetailVo.setReturnSource(returnSource);
        borrowerDetailVo.setContactsRelation(contactsRelation);

        //审批状态
        String status = BorrowerStatusEnum.getMsgByStatus(borrower.getStatus());
        borrowerDetailVo.setStatus(status);

        //获取附件VO列表
        List<BorrowerAttachVo> borrowerAttachVoList =  borrowerAttachService.selectBorrowerAttachVoList(id);
        borrowerDetailVo.setBorrowerAttachVOList(borrowerAttachVoList);

        return borrowerDetailVo;
    }


    /**
     * user——integral表中添加积分明细
     * user-info中添加总积分（user-info表中的原始积分 + user-integral表中的积分明细之和）
     * 修改borrow表的借款申请审核状态
     * 修改user-info表中的借款申请审核状态
     *
     * @param borrowerApprovalVO
     */
    @Transactional(rollbackFor = {Exception.class})
    @Override
    public void approval(BorrowerApprovalVo borrowerApprovalVO) {
        //借款人id
        Long borrowerId = borrowerApprovalVO.getBorrowerId();
        // 借款申请对象
        Borrower borrower = baseMapper.selectById(borrowerId);
        // 设置审核状态
        borrower.setStatus(borrowerApprovalVO.getStatus());
        baseMapper.updateById(borrower);

        // 获取用户id
        Long userId = borrower.getUserId();
        // 获取用户对象
        UserInfo userInfo = userInfoMapper.selectById(userId);

        //用户的原始积分
        Integer integral = userInfo.getIntegral();
        //计算基本信息积分
        UserIntegral userIntegral = new UserIntegral();
        userIntegral.setUserId(userId);
        userIntegral.setIntegral(borrowerApprovalVO.getInfoIntegral());
        userIntegral.setContent("借款人基本信息");
        userIntegralMapper.insert(userIntegral);
        int curIntegral = integral + borrowerApprovalVO.getInfoIntegral();
        //身份证积分
        if(borrowerApprovalVO.getIsIdCardOk()) {
            curIntegral += IntegralEnum.BORROWER_IDCARD.getIntegral();
            userIntegral = new UserIntegral();
            userIntegral.setUserId(userId);
            userIntegral.setIntegral(IntegralEnum.BORROWER_IDCARD.getIntegral());
            userIntegral.setContent(IntegralEnum.BORROWER_IDCARD.getMsg());
            userIntegralMapper.insert(userIntegral);
        }

        //房产积分
        if(borrowerApprovalVO.getIsHouseOk()) {
            curIntegral += IntegralEnum.BORROWER_HOUSE.getIntegral();
            userIntegral = new UserIntegral();
            userIntegral.setUserId(userId);
            userIntegral.setIntegral(IntegralEnum.BORROWER_HOUSE.getIntegral());
            userIntegral.setContent(IntegralEnum.BORROWER_HOUSE.getMsg());
            userIntegralMapper.insert(userIntegral);
        }

        // 车辆积分
        if(borrowerApprovalVO.getIsCarOk()) {
            curIntegral += IntegralEnum.BORROWER_CAR.getIntegral();
            userIntegral = new UserIntegral();
            userIntegral.setUserId(userId);
            userIntegral.setIntegral(IntegralEnum.BORROWER_CAR.getIntegral());
            userIntegral.setContent(IntegralEnum.BORROWER_CAR.getMsg());
            userIntegralMapper.insert(userIntegral);
        }

        // 设置用户总积分
        userInfo.setIntegral(curIntegral);
        //修改审核状态
        userInfo.setBorrowAuthStatus(borrowerApprovalVO.getStatus());
        // 更新userInfo
        userInfoMapper.updateById(userInfo);

    }
}
