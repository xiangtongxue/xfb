package com.xiang.xfb.core.service;

import com.xiang.xfb.core.pojo.entity.TransFlow;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 交易流水表 服务类
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
public interface TransFlowService extends IService<TransFlow> {

}
