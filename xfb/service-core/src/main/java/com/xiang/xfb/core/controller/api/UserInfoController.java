package com.xiang.xfb.core.controller.api;


import com.xiang.common.exception.Assert;
import com.xiang.common.result.R;
import com.xiang.common.result.ResponseEnum;
import com.xiang.common.util.RegexValidateUtils;
import com.xiang.xfb.base.util.JwtUtils;
import com.xiang.xfb.core.pojo.vo.LoginVo;
import com.xiang.xfb.core.pojo.vo.RegisterVo;
import com.xiang.xfb.core.pojo.vo.UserInfoVo;
import com.xiang.xfb.core.service.UserInfoService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 用户基本信息 前端控制器
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
@RestController
@RequestMapping("/api/core/userInfo")
public class UserInfoController {

    @Resource
    private RedisTemplate redisTemplate;

    @Resource
    private UserInfoService userInfoService;

    @ApiOperation("会员注册")
    @PostMapping("/register")
    public R register(@RequestBody RegisterVo registerVo){
        //参数校验
        Assert.notEmpty(registerVo.getMobile(),ResponseEnum.MOBILE_NULL_ERROR);
        Assert.notEmpty(registerVo.getCode(),ResponseEnum.CODE_NULL_ERROR);
        Assert.notEmpty(registerVo.getPassword(),ResponseEnum.PASSWORD_NULL_ERROR);
        Assert.isTrue(RegexValidateUtils.checkCellphone(registerVo.getMobile()),ResponseEnum.MOBILE_ERROR);

        //校验验证码
        String code = registerVo.getCode();
        String codeGen = (String)redisTemplate.opsForValue().get("sms:code:" + registerVo.getMobile());
        Assert.equals(code,codeGen, ResponseEnum.CODE_ERROR);
        //注册
        userInfoService.register(registerVo);

        return R.oK().message("注册成功");
    }

    @ApiOperation("会员登录")
    @PostMapping("/login")
    public R login(@RequestBody LoginVo loginVo, HttpServletRequest request){
        String mobile = loginVo.getMobile();
        String password = loginVo.getPassword();

        Assert.notEmpty(mobile,ResponseEnum.MOBILE_NULL_ERROR);
        Assert.notEmpty(password,ResponseEnum.PASSWORD_NULL_ERROR);

        String ip = request.getRemoteAddr();
        UserInfoVo userInfoVo =  userInfoService.login(loginVo,ip);
        return R.oK().message("登录成功").data("userInfo",userInfoVo);
    }

    @ApiOperation("校验登录")
    @GetMapping("/checkToken")
    public R checkToken( HttpServletRequest request){
        String token = request.getHeader("token");
        if (JwtUtils.checkToken(token)) {
            return R.oK();
        }else {
            return R.setResult(ResponseEnum.LOGIN_AUTH_ERROR);
        }
    }


    @ApiOperation("校验手机号是否注册")
    @GetMapping("/checkMobile/{mobile}")
    public boolean checkMobile(@ApiParam(value = "手机号") @PathVariable("mobile") String mobile){
        boolean result =  userInfoService.checkMobile(mobile);
        return result;
    }
}

