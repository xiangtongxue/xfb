package com.xiang.xfb.core.service;

import com.xiang.xfb.core.pojo.entity.BorrowInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigDecimal;

/**
 * <p>
 * 借款信息表 服务类
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
public interface BorrowInfoService extends IService<BorrowInfo> {

    /**
     * 获取借款额度
     * @param userId 用户id
     * @return 额度
     */
    BigDecimal getBorrowAmount(Long userId);

    /**
     * 获取借款申请审批状态
     * @param userId
     * @return
     */
    Integer getStatusByUserId(Long userId);

    /**
     * 提交借款申请
     * @param borrowInfo
     * @param userId
     */
    void saveBorrowInfo(BorrowInfo borrowInfo, Long userId);
}
