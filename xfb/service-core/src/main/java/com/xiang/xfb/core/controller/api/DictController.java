package com.xiang.xfb.core.controller.api;


import com.alibaba.excel.EasyExcel;
import com.alibaba.fastjson.JSON;
import com.xiang.common.exception.BusinessException;
import com.xiang.common.result.R;
import com.xiang.common.result.ResponseEnum;
import com.xiang.xfb.core.pojo.dto.ExcelDictDTO;
import com.xiang.xfb.core.pojo.entity.Dict;
import com.xiang.xfb.core.service.DictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 数据字典 前端控制器
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
@Api(tags = "数据字段管理")
@Slf4j
@RestController
@RequestMapping("/api/core/dict")
public class DictController {

    @Resource
    DictService dictService;

    @ApiOperation("根据DictCode获取下集结点 ")
    @GetMapping("/findByDictCode/{dictCode}")
    public R findByDictCode( @ApiParam(value = "结点编码") @PathVariable String dictCode){
        List<Dict> dictList =  dictService.findByDictCode(dictCode);
        return R.oK().data("dictList",dictList);
    }


}

