package com.xiang.xfb.core.service;

import com.xiang.xfb.core.pojo.dto.ExcelDictDTO;
import com.xiang.xfb.core.pojo.entity.Dict;
import com.baomidou.mybatisplus.extension.service.IService;

import java.io.InputStream;
import java.util.List;

/**
 * <p>
 * 数据字典 服务类
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
public interface DictService extends IService<Dict> {

    /**
     * 数据导入
     * @param inputStream
     */
    void importData(InputStream inputStream);

    /**
     * 数据导出
     * @return
     */
    List<ExcelDictDTO> listDictData();

    /**
     * 根据上级id获取子节点数据列表
     * @param parentId  上级id
     * @return
     */
    List<Dict> listByParentId(Long parentId);


    /**
     * 根據dictCode获取明细
     * @param dictCode
     * @return
     */
    List<Dict> findByDictCode(String dictCode);

    /**
     * 根據dictCode和value获取名称
     * @param dictCoe
     * @param value
     * @return
     */
    String getNameByParentDictCodeAndValue(String dictCoe, Integer value);
}
