package com.xiang.xfb.core.service.impl;

import com.xiang.xfb.core.pojo.entity.LendReturn;
import com.xiang.xfb.core.mapper.LendReturnMapper;
import com.xiang.xfb.core.service.LendReturnService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 还款记录表 服务实现类
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
@Service
public class LendReturnServiceImpl extends ServiceImpl<LendReturnMapper, LendReturn> implements LendReturnService {

}
