package com.xiang.xfb.core.service;

import com.xiang.xfb.core.pojo.entity.UserAccount;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户账户 服务类
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
public interface UserAccountService extends IService<UserAccount> {

}
