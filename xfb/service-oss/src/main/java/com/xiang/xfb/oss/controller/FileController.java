package com.xiang.xfb.oss.controller;

import com.xiang.common.exception.BusinessException;
import com.xiang.common.result.R;
import com.xiang.common.result.ResponseEnum;
import com.xiang.xfb.oss.service.FileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;

@Api(tags = "阿里云文件管理")
@RestController
@Slf4j
@RequestMapping("/api/oss/file")
public class FileController {


    @Autowired
    private FileService fileService;



    @ApiOperation("文件上传")
    @PostMapping("upload")
    public R upload(@RequestParam("file") MultipartFile file,String module) {
        String url = null;
        try {
            url = fileService.upload(file.getInputStream(), module, file.getOriginalFilename());
        } catch (IOException e) {
            throw new BusinessException(ResponseEnum.UPLOAD_ERROR,e);
        }
        return R.oK().message("文件上传成").data("url",url);
    }

    @ApiOperation("文件删除")
    @DeleteMapping("/remove")
    public R remove(String url){
        fileService.removeFile(url);
        return R.oK();
    }
}

