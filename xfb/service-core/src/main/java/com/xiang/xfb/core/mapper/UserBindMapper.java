package com.xiang.xfb.core.mapper;

import com.xiang.xfb.core.pojo.entity.UserBind;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户绑定表 Mapper 接口
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
public interface UserBindMapper extends BaseMapper<UserBind> {

}
