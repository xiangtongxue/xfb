package com.xiang.xfb.core.service;

import com.xiang.xfb.core.pojo.entity.BorrowerAttach;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiang.xfb.core.pojo.vo.BorrowerAttachVo;

import java.util.List;

/**
 * <p>
 * 借款人上传资源表 服务类
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
public interface BorrowerAttachService extends IService<BorrowerAttach> {

    List<BorrowerAttachVo> selectBorrowerAttachVoList(Long borrowerId);
}
