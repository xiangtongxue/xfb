package com.xiang.xfb.core.mapper;

import com.xiang.xfb.core.pojo.entity.Lend;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 标的准备表 Mapper 接口
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
public interface LendMapper extends BaseMapper<Lend> {

}
