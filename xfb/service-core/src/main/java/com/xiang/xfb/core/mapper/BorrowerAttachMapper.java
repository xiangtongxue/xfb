package com.xiang.xfb.core.mapper;

import com.xiang.xfb.core.pojo.entity.BorrowerAttach;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 借款人上传资源表 Mapper 接口
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
public interface BorrowerAttachMapper extends BaseMapper<BorrowerAttach> {

}
