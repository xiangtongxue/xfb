package com.xiang.xfb.core.mapper;

import com.xiang.xfb.core.pojo.entity.LendReturn;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 还款记录表 Mapper 接口
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
public interface LendReturnMapper extends BaseMapper<LendReturn> {

}
