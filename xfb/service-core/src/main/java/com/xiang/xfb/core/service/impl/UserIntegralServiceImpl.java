package com.xiang.xfb.core.service.impl;

import com.xiang.xfb.core.pojo.entity.UserIntegral;
import com.xiang.xfb.core.mapper.UserIntegralMapper;
import com.xiang.xfb.core.service.UserIntegralService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户积分记录表 服务实现类
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
@Service
public class UserIntegralServiceImpl extends ServiceImpl<UserIntegralMapper, UserIntegral> implements UserIntegralService {

}
