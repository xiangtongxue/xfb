package com.xiang.xfb.core.mapper;

import com.xiang.xfb.core.pojo.entity.BorrowInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 借款信息表 Mapper 接口
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
public interface BorrowInfoMapper extends BaseMapper<BorrowInfo> {

}
