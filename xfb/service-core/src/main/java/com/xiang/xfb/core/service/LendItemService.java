package com.xiang.xfb.core.service;

import com.xiang.xfb.core.pojo.entity.LendItem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 标的出借记录表 服务类
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
public interface LendItemService extends IService<LendItem> {

}
