package com.xiang.xfb.core.mapper;

import com.xiang.xfb.core.pojo.entity.UserIntegral;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户积分记录表 Mapper 接口
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
public interface UserIntegralMapper extends BaseMapper<UserIntegral> {

}
