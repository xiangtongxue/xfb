package com.xiang.xfb.core.service.impl;

import com.xiang.xfb.core.pojo.entity.TransFlow;
import com.xiang.xfb.core.mapper.TransFlowMapper;
import com.xiang.xfb.core.service.TransFlowService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 交易流水表 服务实现类
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
@Service
public class TransFlowServiceImpl extends ServiceImpl<TransFlowMapper, TransFlow> implements TransFlowService {

}
