package com.xiang.xfb.core.controller.admin;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiang.common.result.R;
import com.xiang.xfb.base.util.JwtUtils;
import com.xiang.xfb.core.pojo.entity.Borrower;
import com.xiang.xfb.core.pojo.vo.BorrowerApprovalVo;
import com.xiang.xfb.core.pojo.vo.BorrowerDetailVo;
import com.xiang.xfb.core.pojo.vo.BorrowerVo;
import com.xiang.xfb.core.service.BorrowerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Api(tags = "借款人管理")
@RestController
@RequestMapping("/admin/core/borrower")
@Slf4j
public class AdminBorrowerController {

    @Resource
    private BorrowerService borrowerService;

    @ApiOperation("获取借款人分页列表")
    @GetMapping("/list/{page}/{limit}")
    public R listPage(
            @ApiParam(value = "当前页码", required = true)
            @PathVariable Long page,

            @ApiParam(value = "每页记录数", required = true)
            @PathVariable Long limit,

            @ApiParam(value = "查询关键字", required = false)
            @RequestParam String keyword) {
        //这里的@RequestParam其实是可以省略的，但是在目前的swagger版本中（2.9.2）不能省略，
        //否则默认将没有注解的参数解析为body中的传递的数据

        Page<Borrower> pageParam = new Page<>(page, limit);
        IPage<Borrower> pageModel = borrowerService.listPage(pageParam, keyword);
        return R.oK().data("pageModel", pageModel);
    }


    @ApiOperation("获取借款人信息")
    @GetMapping("/show/{id}")
    public R show(
            @ApiParam(value = "借款人id", required = true)
            @PathVariable Long id) {
        BorrowerDetailVo borrowerDetailVo = borrowerService.getBorrowerDetailVOById(id);
        return R.oK().data("borrowerDetailVo", borrowerDetailVo);
    }


    @ApiOperation("借款额度审批")
    @PostMapping("/approval")
    public R approval(@RequestBody BorrowerApprovalVo borrowerApprovalVO) {
        borrowerService.approval(borrowerApprovalVO);
        return R.oK().message("审批完成");
    }
}
