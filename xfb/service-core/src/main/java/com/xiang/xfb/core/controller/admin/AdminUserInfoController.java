package com.xiang.xfb.core.controller.admin;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiang.common.exception.Assert;
import com.xiang.common.result.R;
import com.xiang.common.result.ResponseEnum;
import com.xiang.common.util.RegexValidateUtils;
import com.xiang.xfb.base.util.JwtUtils;
import com.xiang.xfb.core.pojo.entity.UserInfo;
import com.xiang.xfb.core.pojo.query.UserInfoQuery;
import com.xiang.xfb.core.pojo.vo.LoginVo;
import com.xiang.xfb.core.pojo.vo.RegisterVo;
import com.xiang.xfb.core.pojo.vo.UserInfoVo;
import com.xiang.xfb.core.service.UserInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 用户基本信息 前端控制器
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
@Api(tags = "会员管理")
@RestController
@RequestMapping("/admin/core/userInfo")
public class AdminUserInfoController {


    @Resource
    private UserInfoService userInfoService;

    @ApiOperation("获取会员分页列表")
    @GetMapping("/list/{page}/{limit}")
    public R listPage(
            @ApiParam(value = "当前页码", required = true)
            @PathVariable Long page,

            @ApiParam(value = "每页记录数", required = true)
            @PathVariable Long limit,

            @ApiParam(value = "查询对象", required = false)
                    UserInfoQuery userInfoQuery) {

        Page<UserInfo> pageParam = new Page<>(page, limit);
        IPage<UserInfo> pageModel = userInfoService.listPage(pageParam, userInfoQuery);
        return R.oK().data("pageModel", pageModel);
    }


    @ApiOperation("锁定/解锁")
    @PutMapping("/lock/{id}/{status}")
    public R lock(@ApiParam(value = "用户id",required = true) @PathVariable("id") Long id,
                  @ApiParam(value = "锁定状态（1正常 0 锁定）",required = true)@PathVariable("status") Integer status){
        userInfoService.lock(id,status);
        return R.oK().message(status == 1?"解锁成功":"锁定成功");
    }


}

