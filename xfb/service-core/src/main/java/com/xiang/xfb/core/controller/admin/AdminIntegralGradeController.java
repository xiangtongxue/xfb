package com.xiang.xfb.core.controller.admin;


import com.xiang.common.exception.BusinessException;
import com.xiang.common.result.R;
import com.xiang.common.result.ResponseEnum;
import com.xiang.xfb.core.pojo.entity.IntegralGrade;
import com.xiang.xfb.core.service.IntegralGradeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 积分等级表 前端控制器
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
@Slf4j
@Api(tags = "积分登记管理")
@RestController
@RequestMapping("/admin/core/integralGrade")
public class AdminIntegralGradeController {

    @Resource
    private IntegralGradeService integralGradeService;

    @ApiOperation(value = "获取列表")
    @GetMapping("/list")
    public R listAll(){
        log.info("info log");
        log.warn("warn log");
        log.error("error log");
        List<IntegralGrade> integralGradeList = integralGradeService.list();
        return R.oK().data("list",integralGradeList).message("获取积分列表成功");
    }

    @ApiOperation(value = "根据id删除" ,notes = "逻辑删除数据记录")
    @DeleteMapping("/remove/{id}")
    public R removeById(
            @ApiParam(value = "数据id",example = "1",required = true)
            @PathVariable Long id){
        boolean result = integralGradeService.removeById(id);
        if (result){
            return R.oK().message("删除成功");
        }
        return R.error().message("删除失败");
    }


    @ApiOperation(value = "新增积分等级")
    @PostMapping("/save")
    public R save(
            @ApiParam("积分等级对象")
            @RequestBody IntegralGrade integralGrade){
        if (integralGrade == null){
            throw new BusinessException(ResponseEnum.SERVLET_ERROR);
        }
        integralGradeService.save(integralGrade);
        return R.oK().message("添加成功");
    }


    @ApiOperation("根据id获取积分等级")
    @GetMapping("/get/{id}")
    public R getById(@PathVariable Long id){
        IntegralGrade integralGrade = integralGradeService.getById(id);
        if (integralGrade !=null){
            return R.oK().message("获取成功").data("record",integralGrade);
        }
        return R.error().message("获取失败");
    }


    @ApiOperation(value = "修改积分等级")
    @PutMapping("/update")
    public R update(
            @ApiParam("积分等级对象")
            @RequestBody IntegralGrade integralGrade){
        integralGradeService.updateById(integralGrade);
        return R.oK().message("修改成功");
    }

}

