package com.xiang.xfb.core.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiang.xfb.core.pojo.entity.Borrower;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiang.xfb.core.pojo.vo.BorrowerApprovalVo;
import com.xiang.xfb.core.pojo.vo.BorrowerDetailVo;
import com.xiang.xfb.core.pojo.vo.BorrowerVo;

/**
 * <p>
 * 借款人 服务类
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
public interface BorrowerService extends IService<Borrower> {

    void saveBorrowerVoByUserId(BorrowerVo borrowerVo, Long userId);

    Integer getStatusByUserId(Long userId);

    IPage<Borrower> listPage(Page<Borrower> pageParam, String keyword);

    BorrowerDetailVo getBorrowerDetailVOById(Long id);

    /**
     * 借款额度审核
     * @param borrowerApprovalVO
     */
    void approval(BorrowerApprovalVo borrowerApprovalVO);
}
