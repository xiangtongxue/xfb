package com.xiang.xfb.core.service.impl;

import com.xiang.xfb.core.pojo.entity.IntegralGrade;
import com.xiang.xfb.core.mapper.IntegralGradeMapper;
import com.xiang.xfb.core.service.IntegralGradeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 积分等级表 服务实现类
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
@Service
public class IntegralGradeServiceImpl extends ServiceImpl<IntegralGradeMapper, IntegralGrade> implements IntegralGradeService {

}
