package com.xiang.xfb.core.mapper;

import com.xiang.xfb.core.pojo.entity.IntegralGrade;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 积分等级表 Mapper 接口
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
public interface IntegralGradeMapper extends BaseMapper<IntegralGrade> {

}
