package com.xiang.xfb.core.service.impl;

import com.xiang.xfb.core.pojo.entity.Lend;
import com.xiang.xfb.core.mapper.LendMapper;
import com.xiang.xfb.core.service.LendService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 标的准备表 服务实现类
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
@Service
public class LendServiceImpl extends ServiceImpl<LendMapper, Lend> implements LendService {

}
