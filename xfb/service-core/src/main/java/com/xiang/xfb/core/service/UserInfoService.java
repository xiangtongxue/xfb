package com.xiang.xfb.core.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiang.xfb.core.pojo.entity.UserInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiang.xfb.core.pojo.query.UserInfoQuery;
import com.xiang.xfb.core.pojo.vo.LoginVo;
import com.xiang.xfb.core.pojo.vo.RegisterVo;
import com.xiang.xfb.core.pojo.vo.UserInfoVo;

/**
 * <p>
 * 用户基本信息 服务类
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
public interface UserInfoService extends IService<UserInfo> {

    void register(RegisterVo registerVo);

    UserInfoVo login(LoginVo loginVo, String ip);

    IPage<UserInfo> listPage(Page<UserInfo> pageParam, UserInfoQuery userInfoQuery);

    void  lock(Long id,Integer status);

    boolean checkMobile(String mobile);
}
