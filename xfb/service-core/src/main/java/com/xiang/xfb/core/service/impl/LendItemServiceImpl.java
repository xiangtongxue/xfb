package com.xiang.xfb.core.service.impl;

import com.xiang.xfb.core.pojo.entity.LendItem;
import com.xiang.xfb.core.mapper.LendItemMapper;
import com.xiang.xfb.core.service.LendItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 标的出借记录表 服务实现类
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
@Service
public class LendItemServiceImpl extends ServiceImpl<LendItemMapper, LendItem> implements LendItemService {

}
