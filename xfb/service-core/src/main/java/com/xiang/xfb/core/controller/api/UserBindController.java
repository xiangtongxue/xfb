package com.xiang.xfb.core.controller.api;

import com.alibaba.fastjson.JSON;
import com.xiang.common.result.R;
import com.xiang.xfb.base.util.JwtUtils;
import com.xiang.xfb.core.hfb.RequestHelper;
import com.xiang.xfb.core.pojo.vo.UserBindVo;
import com.xiang.xfb.core.service.UserBindService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Api(tags = "会员账号绑定")
@RestController
@RequestMapping("/api/core/userBind")
@Slf4j
public class UserBindController {

    @Resource
    private UserBindService userBindService;

    @ApiOperation("账户绑定提交数据")
    @PostMapping("/auth/bind")
    public com.xiang.common.result.R bind(@RequestBody UserBindVo userBindVo, HttpServletRequest request) {
        String token = request.getHeader("token");
        //token中获取userId
        Long userId = JwtUtils.getUserId(token);
        //根据userId做用户绑定，生成动态表单字符串
        String formStr = userBindService.commitBindUser(userBindVo, userId);
        return R.oK().data("formStr", formStr);
    }

    @ApiOperation("账户绑定异步回调")
    @PostMapping("/notify")
    public String notify(HttpServletRequest request) {
        Map<String, Object> paramMap = RequestHelper.switchMap(request.getParameterMap());
        log.info("用户账号绑定异步回调：" + JSON.toJSONString(paramMap));

        //校验签名
        if(!RequestHelper.isSignEquals(paramMap)) {
            log.error("用户账号绑定异步回调签名错误：" + JSON.toJSONString(paramMap));
            return "fail";
        }

        //修改绑定状态
        userBindService.notify(paramMap);
        return "success";
    }
}
