package com.xiang.xfb.core.service;

import com.xiang.xfb.core.pojo.entity.Lend;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 标的准备表 服务类
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
public interface LendService extends IService<Lend> {

}
