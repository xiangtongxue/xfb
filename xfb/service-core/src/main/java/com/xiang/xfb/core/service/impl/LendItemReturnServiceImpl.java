package com.xiang.xfb.core.service.impl;

import com.xiang.xfb.core.pojo.entity.LendItemReturn;
import com.xiang.xfb.core.mapper.LendItemReturnMapper;
import com.xiang.xfb.core.service.LendItemReturnService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 标的出借回款记录表 服务实现类
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
@Service
public class LendItemReturnServiceImpl extends ServiceImpl<LendItemReturnMapper, LendItemReturn> implements LendItemReturnService {

}
