package com.xiang.xfb.core.mapper;

import com.xiang.xfb.core.pojo.dto.ExcelDictDTO;
import com.xiang.xfb.core.pojo.entity.Dict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 数据字典 Mapper 接口
 * </p>
 *
 * @author xj
 * @since 2022-02-27
 */
public interface DictMapper extends BaseMapper<Dict> {

    /**
     * 批量保存
     * @param list
     */
    void insertBatch(List<ExcelDictDTO> list);
}
